install-base-dependencies:
	pip install -r requirements/base.txt

install-development-dependencies: install-base-dependencies
	pip install -r requirements/development.txt


run-celery-worker:
	celery -A hello_celery.tasks worker --loglevel=info

run-client:
	python hello_celery/client.py
