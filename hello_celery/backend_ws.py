import asyncio
import json
import websockets

from aioredis import create_connection, Channel


async def subscribe_to_redis(path):
    conn = await create_connection(('localhost', 6379))
    channel = Channel("celery-task-meta-*", is_pattern=True)
    await conn.execute_pubsub('psubscribe', channel)
    return channel, conn


async def browser_server(websocket, path):
    channel, conn = await subscribe_to_redis(path)
    try:
        while True:
            message = await channel.get()
            print(message)
            await websocket.send(json.loads(message[1])["result"])

    except websockets.exceptions.ConnectionClosed:
        await conn.execute_pubsub('punsubscribe', channel)
        conn.close()

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.set_debug(True)
    ws_server = websockets.serve(browser_server, 'localhost', 5678)
    loop.run_until_complete(ws_server)
    loop.run_forever()
