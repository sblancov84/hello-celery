import redis
import json


class Backend():

    def __init__(self):
        self.backend = redis.Redis(host="localhost", port=6379, db=1)

    def polling(self):
        results = map(
            lambda key: self.__get_value(key)['result'],
            self.backend.keys('*')
        )
        return results

    def __get_value(self, key):
        raw_value = self.backend.get(key)
        value = json.loads(raw_value)
        return value


def polling():
    backend = Backend()
    results = backend.polling()
    for result in results:
        print(result)


if __name__ == "__main__":
    polling()
