import json
import redis

server = redis.Redis(host="localhost", port=6379, db=1)


def process():
    """Process messages from the pubsub stream."""
    ps = server.pubsub()
    channel = "celery-task-meta-*"
    ps.psubscribe(channel)
    for raw_message in ps.listen():
        if raw_message["type"] != "pmessage":
            continue
        message = json.loads(raw_message["data"])
        process_message(message)


def process_message(message):
    print(message['result'])


if __name__ == "__main__":
    process()
