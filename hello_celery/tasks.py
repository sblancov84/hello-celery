import requests

from celery import Celery


BROKER_URL = 'redis://localhost:6379/0'
BACKEND_URL = 'redis://localhost:6379/1'
app = Celery('tasks', broker=BROKER_URL, backend=BACKEND_URL)


@app.task(bind=True)
def request_http(x, url):
    result = requests.get(url)
    return result.text
