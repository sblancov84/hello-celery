import redis

server = redis.Redis(host="localhost", port=6379, db=1)

with server.monitor() as monitor:
    for command in monitor.listen():
        print(command)
