# Hello Celery

This is a hello world project for the first contact with Celery (again).

## Setup Development environment

### Dependencies management

Install them:

    pip install -r requirements/base.txt
    pip install -r requirements/development.txt

Add a new one:

Use the next pattern:

    library_name==installed_version

* Use requirements/base.txt if dependency is for runtime.
* Use requirements/development.txt if dependency is an auxiliar tool or library
  for development.


### Virtual environment management

Create:

    pyenv virtualenv 3.7.3 hello-celery

Activate:

    pyenv activate hello-celery

Deactivate:

    pyenv deactivate

### Analysis

    flake8

### Broker setup

Celery can use different brokers. I have selected one and I have followed
[Using Redis](https://docs.celeryproject.org/en/latest/getting-started/brokers/redis.html)
instructions in a virtual machine to start to work.

Install Redis:

    sudo apt-get install redis-server   # We are working in Debian 10

Then, we must edit configuration file "/etc/redis/redis.conf" and make the
following changes:

* comment the line where "bind 127.0.0.1 ::1"  is written
* "protected-method no" instead of "protected-method yes"

NOTE: This changes are insecure and it should not used in production, but for
testing purposes are ok.

Now, we can test redis from our host machine:

    echo 'PING' | nc 192.168.33.10 6379

It should work.

# Proofs of Concept

There are some of interesting files in this repository:
* tasks
* productor
* consumer
* monitor
* pubsub

## Tasks

This file contains a task that do a simple HTTP request to google.

## Productor

It is a script that sends tasks to the broker (redis).

## Consumer

It is a script that execute a task waiting for the result of that task in the same
process.

## Monitor

Just listen for all events in Redis.

## Pubsub

In this case, it is a subscriptor to get all results of celery tasks.

# References

An interesting article with multiple patterns:
https://yeti.co/blog/establishing-a-websocket-pubsub-server-with-redis-and-asyncio-for-the-light-sensor/


# How to use!

1. Start redis
1. Start celery worker at hello_celery

    celery -A tasks worker --loglevel=info

1. Start backend_ws.py

    python hello_celery/backend_ws.py

1. Open client.html into webbrowser

    google-chrome hello_celery/client.html

1. Start a producer, multiples times to watch how to "Hello world!" is coming to browser :)

    python hello_celery/producer.py
